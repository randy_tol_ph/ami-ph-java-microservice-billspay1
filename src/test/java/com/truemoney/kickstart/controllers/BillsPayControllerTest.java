package com.truemoney.kickstart.controllers;

import com.truemoney.kickstart.models.dto.BillerListResponse;
import com.truemoney.kickstart.models.dto.BillsPayRequest;
import com.truemoney.kickstart.models.dto.BillsPayResponse;
import com.truemoney.kickstart.services.BillsPaySample;
import com.truemoney.kickstart.services.micro.ECPayMS;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import wsbillpay.wsdl.ArrayOfBStruct;
import wsbillpay.wsdl.BStruct;
import wsbillpay.wsdl.GetBillerListResponse;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillsPayControllerTest {
    @InjectMocks
    private BillsPayController billsPayController;

    @Mock
    BillsPaySample billsPaySample;

    @Mock
    private ECPayMS ecPayMS;

    @Mock
    GetBillerListResponse getBillerListResponse;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGet() {
        BillsPayController billsPayController = new BillsPayController();
        assertEquals("hola", billsPayController.get());
    }

    @Test
    public void testSample() {
        BillsPayResponse billsPayResponse = new BillsPayResponse();

        BillsPayRequest billsPayRequest = new BillsPayRequest();
        billsPayRequest.setRefNumber("RNDM1000001");
        billsPayRequest.setBillerCode("RNDMBLRCD01");
        billsPayRequest.setAmount(100);

        JSONObject[] billerInfos = {new JSONObject()};
        billsPayRequest.setBillerInfos(billerInfos);

        when(billsPaySample.billRequest(billsPayRequest)).thenReturn(billsPayResponse);

        BillsPayResponse test = billsPayController.sample(billsPayRequest);

        verify(billsPaySample).billRequest(billsPayRequest);
        assertEquals(billsPayResponse, test);
    }

    @Test
    public void testGetBillerList() {
        when(ecPayMS.getBillerList()).thenReturn(getBillerListResponse);
        when(getBillerListResponse.getGetBillerListResult()).thenReturn(new ArrayOfBStruct());

        BillerListResponse billerListResponse = new BillerListResponse();
        billerListResponse.setResponseMessage("Success");
        billerListResponse.setResponseCode(1);
        billerListResponse.setData(new ArrayList<BStruct>());

        BillerListResponse test = billsPayController.getBillerList();

        verify(ecPayMS).getBillerList();
        verify(getBillerListResponse).getGetBillerListResult();
        assertEquals(billerListResponse.getResponseMessage(), test.getResponseMessage());
        assertEquals(billerListResponse.getResponseCode(), test.getResponseCode());
        assertEquals(billerListResponse.getData(), test.getData());
    }
}
