package com.truemoney.kickstart.services;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

import com.truemoney.kickstart.models.BillsPay;
import com.truemoney.kickstart.models.dto.BillsPayRequest;
import com.truemoney.kickstart.models.dto.BillsPayResponse;
import com.truemoney.kickstart.models.dto.BillsPayResponseData;
import com.truemoney.kickstart.repositories.BillsPayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillsPaySampleImpl implements BillsPaySample {

    @Autowired
    BillsPayRepository billsPayRepo;

    @Override
    public BillsPayResponse billRequest(BillsPayRequest billsPayRequest) {
        BillsPayResponse billsPayResponse = new BillsPayResponse();
        BillsPayResponseData billsPayResponseData = new BillsPayResponseData();
        billsPayResponseData.setBillRefNumber("sample");

        //save to repo
        BillsPay billsPay = new BillsPay();
        billsPay.setRef_number(billsPayRequest.getRefNumber());
        billsPay.setAmount(billsPayRequest.getAmount());
        billsPay.setBiller_code(billsPayRequest.getBillerCode());
        billsPay.setBiller_infos(billsPayRequest.getBillerInfos());
        billsPay.setBill_ref_number("sample");

        billsPayResponse.setData(billsPayResponseData);
        billsPayResponse.setResponseCode(1);
        billsPayResponse.setResponseMessage("Success");

        billsPayRepo.save(billsPay);

        return billsPayResponse;
    }
}