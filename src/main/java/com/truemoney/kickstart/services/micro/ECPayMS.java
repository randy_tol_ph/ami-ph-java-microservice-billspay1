package com.truemoney.kickstart.services.micro;

/**
 * author randy_tol_ph
 * Date: 07/31/2018
 */

import wsbillpay.wsdl.GetBillerListResponse;

public interface ECPayMS {
    GetBillerListResponse getBillerList();
}
