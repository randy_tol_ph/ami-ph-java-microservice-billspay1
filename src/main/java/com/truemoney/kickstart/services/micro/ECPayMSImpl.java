package com.truemoney.kickstart.services.micro;

/**
 * author randy_tol_ph
 * Date: 07/31/2018
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import wsbillpay.wsdl.GetBillerList;
import wsbillpay.wsdl.GetBillerListResponse;

@Service
public class ECPayMSImpl extends WebServiceGatewaySupport implements ECPayMS {

    private static final Logger log = LoggerFactory.getLogger(ECPayMSImpl.class);

    @Autowired
    private Environment env;

    private String ACCOUNTID;
    private String USERNAME;
    private String PASSWORD;

    @Override
    public GetBillerListResponse getBillerList() {
        ACCOUNTID = env.getProperty("ecp_accountID");
        USERNAME = env.getProperty("ecp_userName");
        PASSWORD = env.getProperty("ecp_password");

        GetBillerList getBillerList = new GetBillerList();
        getBillerList.setAccountID(ACCOUNTID);
        getBillerList.setPassword(PASSWORD);
        getBillerList.setUsername(USERNAME);

        log.info("Getting Biller List with infos: " + ACCOUNTID + ": " + PASSWORD + ": " + USERNAME);

        GetBillerListResponse response = (GetBillerListResponse) getWebServiceTemplate()
                .marshalSendAndReceive("https://ecpay.ph/wsbillpay/Service1.asmx?wsdl", getBillerList,
                        new SoapActionCallback("http://tempuri.org/ECPNBillsPayment/Service1/GetBillerList"));
        return response;
    }
}