package com.truemoney.kickstart.services;

import com.truemoney.kickstart.models.dto.BillsPayRequest;
import com.truemoney.kickstart.models.dto.BillsPayResponse;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

public interface BillsPaySample {
    BillsPayResponse billRequest(BillsPayRequest billsPayRequest);
}