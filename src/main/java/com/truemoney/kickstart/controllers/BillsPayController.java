package com.truemoney.kickstart.controllers;

/**
 * @author randy_tol_ph
 */

import com.truemoney.kickstart.models.dto.BillerListResponse;
import com.truemoney.kickstart.models.dto.BillsPayRequest;
import com.truemoney.kickstart.models.dto.BillsPayResponse;
import com.truemoney.kickstart.services.BillsPaySample;
import com.truemoney.kickstart.services.micro.ECPayMS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import wsbillpay.wsdl.BStruct;
import wsbillpay.wsdl.GetBillerListResponse;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/billsPay/V1/")
@Api(value = "billsPay/V1/", description = "Operations pertaining to Bills Payment Micro Service.")
public class BillsPayController {
    @Autowired
    private BillsPaySample billsPaySample;

    @Autowired
    private ECPayMS ecPayMS;

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public String get() {
        return "hola";
    }

    @ApiOperation(value = "A sample POST payment passing Bill RefNum, Amount, Biller Code and Biller Infos. This post also writes record to the database depending on operations result if passed/failed.", response = BillsPayResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "sample", method = RequestMethod.POST, produces = "application/json")
    public BillsPayResponse sample(@RequestBody BillsPayRequest billsPayRequest) {
        return billsPaySample.billRequest(billsPayRequest);
    }

    @ApiOperation(value = "View all the Biller Categories available for selection from EC Pay.", response = BillerListResponse.class)
    @RequestMapping(value = "getBillerList", method = RequestMethod.GET, produces = "application/json")
    public BillerListResponse getBillerList() {
        BillerListResponse billerListResponse = new BillerListResponse();

        GetBillerListResponse result = ecPayMS.getBillerList();
        List<BStruct> billerList = new ArrayList<BStruct>();

        for (BStruct bStruct : result.getGetBillerListResult().getBStruct()) {
            billerList.add(bStruct);
        }

        billerListResponse.setResponseCode(1);
        billerListResponse.setResponseMessage("Success");
        billerListResponse.setData(billerList);

        return billerListResponse;
    }
}