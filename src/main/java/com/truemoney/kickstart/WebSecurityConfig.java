package com.truemoney.kickstart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private Environment env;

    private boolean disableAppLogin = false;
    private int serverPort = 8080;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (env.getProperty("disable-app.login") != null && !env.getProperty("disable-app.login").isEmpty()) {
            disableAppLogin = Boolean.valueOf(env.getProperty("disable-app.login"));
        }

        if (!disableAppLogin) {
            http
                    .authorizeRequests()
                    .antMatchers("/resources/**").permitAll()
                    .antMatchers("/manage/**").hasAnyRole("MANAGER", "SUPERUSER")
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .and()
                    .logout()
                    .permitAll();
        } else {
            http
                    .authorizeRequests()
                    .anyRequest().permitAll();
        }

        //for csrf
        /*http
                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
                .csrf().csrfTokenRepository(csrfTokenRepository());*/

        http.csrf().disable();
    }

    /*private CsrfTokenRepository csrfTokenRepository(){
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }*/
}