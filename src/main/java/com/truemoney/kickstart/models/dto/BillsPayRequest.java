package com.truemoney.kickstart.models.dto;

import org.json.simple.JSONObject;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

public class BillsPayRequest {

    private String refNumber;

    private double amount;

    private String billerCode;

    private JSONObject[] billerInfos;

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public JSONObject[] getBillerInfos() {
        return billerInfos;
    }

    public void setBillerInfos(JSONObject[] billerInfos) {
        this.billerInfos = billerInfos;
    }
}



