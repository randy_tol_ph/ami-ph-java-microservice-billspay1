package com.truemoney.kickstart.models.dto;

import com.truemoney.kickstart.models.generic.ResponseObject;
import wsbillpay.wsdl.BStruct;

import java.util.List;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

public class BillerListResponse extends ResponseObject<List<BStruct>> {
    @Override
    public List<BStruct> getData() {
        return (List<BStruct>) this.data;
    }

    @Override
    public void setData(List<BStruct> data) {
        this.data = data;
    }
}