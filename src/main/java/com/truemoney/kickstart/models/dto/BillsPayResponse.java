package com.truemoney.kickstart.models.dto;

import com.truemoney.kickstart.models.generic.ResponseObject;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

public class BillsPayResponse extends ResponseObject<BillsPayResponseData> {
    @Override
    public BillsPayResponseData getData() {
        return (BillsPayResponseData) this.data;
    }

    @Override
    public void setData(BillsPayResponseData data) {
        this.data = data;
    }
}