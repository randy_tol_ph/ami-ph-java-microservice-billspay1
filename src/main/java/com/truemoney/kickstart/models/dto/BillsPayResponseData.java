package com.truemoney.kickstart.models.dto;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

public class BillsPayResponseData {

    private String billRefNumber;

    public String getBillRefNumber() {
        return billRefNumber;
    }

    public void setBillRefNumber(String billRefNumber) {
        this.billRefNumber = billRefNumber;
    }
}