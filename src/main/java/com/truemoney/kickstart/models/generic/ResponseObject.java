package com.truemoney.kickstart.models.generic;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

public abstract class ResponseObject<E> {

    public Object data;

    private int responseCode;

    private String responseMessage;

    public abstract E getData();

    public abstract void setData(E data);

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}