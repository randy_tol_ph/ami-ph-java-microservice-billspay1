package com.truemoney.kickstart;

/**
 * author randy_tol_ph
 * Date: 07/31/2018
 */

import com.truemoney.kickstart.services.micro.ECPayMSImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class BillsPayWSConfiguration {
    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("wsbillpay.wsdl");
        return marshaller;
    }

    @Bean
    @Primary
    public ECPayMSImpl ecPayMSBillsPay(Jaxb2Marshaller marshaller) {
        ECPayMSImpl consumer = new ECPayMSImpl();

        consumer.setDefaultUri("https://ecpay.ph/wsbillpay");
        consumer.setMarshaller(marshaller);
        consumer.setUnmarshaller(marshaller);
        return consumer;
    }
}