package com.truemoney.kickstart.repositories;

import com.truemoney.kickstart.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rltolentino on 2/9/2017.
 */
public interface UserRepository extends JpaRepository<Users, Long> {
    Users findByUsername(String username);

    Users findByFnameAndLname(String fname, String lname);
}
