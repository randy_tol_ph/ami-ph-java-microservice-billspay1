package com.truemoney.kickstart.repositories;

/**
 * author randy_tol_ph
 * Date: 07/23/2018
 */

import com.truemoney.kickstart.models.BillsPay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillsPayRepository extends JpaRepository<BillsPay, Long> {

}