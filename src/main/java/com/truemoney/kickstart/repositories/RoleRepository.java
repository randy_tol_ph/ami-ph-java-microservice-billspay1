package com.truemoney.kickstart.repositories;

import com.truemoney.kickstart.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by rltolentino on 2/9/2017.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    List<Role> findAll();

    Role findByName(String name);
}
